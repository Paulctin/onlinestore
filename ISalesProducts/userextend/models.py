from django.contrib.auth.models import User
from django.db import models


class ExtendUser(User):
    gender_options = (('male', 'Male'), ('female', 'Female'), ('other', 'Other'))

    age = models.IntegerField()
    phone_number = models.CharField(max_length=15)
    gender = models.CharField(choices=gender_options, max_length=6)
    birth_date = models.DateField()

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
