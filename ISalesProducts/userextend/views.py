from django.conf.global_settings import EMAIL_HOST_USER
from django.core.mail import send_mail, EmailMessage
from django.shortcuts import redirect
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.views.generic import CreateView

from userextend.forms import UserForm
from userextend.models import ExtendUser


class UserCreateView(CreateView):
    template_name = 'userextend/create_user.html'
    model = ExtendUser
    form_class = UserForm
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        if form.is_valid() and not form.errors:
            new_user = form.save(commit=False)
            new_user.first_name = new_user.first_name.title()
            new_user.username = f'{new_user.first_name.lower()}{new_user.last_name.lower()}'

            new_user.save()
            subject = 'S-a creat cont nou'
            message = f'Felicitari, ti-ai creat contul. \n Numele de utilizator este: {new_user.username}'

            send_mail(subject, message, EMAIL_HOST_USER, [new_user.email])


            details_user = {
                'fullname': f'{new_user.first_name} {new_user.last_name}',
                'user_name': f'{new_user.username}'
            }
            message = get_template('mail_user.html').render(details_user)
            subject = 'User Registration Successfully'

            mail = EmailMessage(
                subject,
                message,
                EMAIL_HOST_USER,
                [new_user.email]
            )
            mail.content_subtype = 'html'
            mail.send()

        return redirect('login')

