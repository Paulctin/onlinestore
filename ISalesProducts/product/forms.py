from django import forms
from django.forms import Select, TextInput, Textarea, FileInput

from product.models import Product


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = {'name', 'price', 'description','category', 'image'}

        widgets = {
            'image' : FileInput(attrs={"placeholder": "Please upload an image for this product", "class": "form-control"}),
            'category': Select(attrs={'class': 'form-select'}),
            'name': TextInput(attrs={'placeholder': 'Please enter the product name', 'class': 'form-control'}),
            'price': TextInput(attrs={'placeholder': 'Please enter the price of the product', 'class': 'form-control'}),
            'description': Textarea(attrs={'placeholder': 'Please enter the description of the product', 'class': 'form-control'}),
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        get_name = cleaned_data.get('name')
        all_names = Product.objects.all()
        for product_name in all_names:
            if product_name.name == get_name:
                msg = 'This product is registered in the database'
                self._errors['name'] = self.error_class([msg])

        return cleaned_data


class ProductUpdateForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = {'category', 'name', 'price', 'description', 'image'}

        widgets = {
            'image': FileInput(attrs={"class": "form-control"}),
            'category': Select(attrs={'class': 'form-select'}),
            'name': TextInput(attrs={'placeholder': 'Please enter the product name', 'class': 'form-control'}),
            'price': TextInput(attrs={'placeholder': 'Please enter the price of the product', 'class': 'form-control'}),
            'description': Textarea(attrs={'placeholder': 'Please enter the description of the product', 'class': 'form-control'}),
        }

