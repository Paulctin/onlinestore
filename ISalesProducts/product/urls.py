from django.urls import path

from product import views

urlpatterns = [
    path('create_product/', views.CreateProductView.as_view(), name='create-product'),
    path('list_of_product/', views.ProductListView.as_view(), name='list-of-products'),
    path('update_product/<int:pk>', views.UpdateProductView.as_view(), name='update-product'),
    path('delete_product/<int:pk>', views.DeleteProductView.as_view(), name='delete-product'),
    path('details_product/<int:pk>', views.DetailsProductView.as_view(), name='details-product'),
]