from django.db import models

from category.models import Category


class Product(models.Model):
    category = models.ForeignKey(to=Category, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=50)
    price = models.CharField(max_length=30)
    description = models.TextField(max_length=100)
    image = models.FileField(upload_to='photos/', null=True)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.name} from {self.category.name}'

