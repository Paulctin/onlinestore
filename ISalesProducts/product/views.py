from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView, TemplateView

import product
from product.forms import ProductForm, ProductUpdateForm
from product.models import Product


class CreateProductView(LoginRequiredMixin,PermissionRequiredMixin, CreateView):
    template_name = 'product/create_product.html'
    model = Product
    form_class = ProductForm
    success_url = reverse_lazy('list-of-products')
    permission_required = 'product.add_product'


class ProductListView(ListView):
    template_name = 'product/list_of_products.html'
    model = Product
    context_object_name = 'products'


class UpdateProductView(LoginRequiredMixin,PermissionRequiredMixin, UpdateView):
    template_name = 'product/update_product.html'
    model = Product
    form_class = ProductUpdateForm
    success_url = reverse_lazy('list-of-products')
    permission_required = 'product.change_product'


class DeleteProductView(LoginRequiredMixin,PermissionRequiredMixin, DeleteView):
    template_name = 'product/delete_product.html'
    model = Product
    success_url = reverse_lazy('list-of-products')
    permission_required = 'product.delete_product'


class DetailsProductView(LoginRequiredMixin, DetailView):
    template_name = 'product/details_product.html'
    model = Product

    def get_context_data(self, **kwargs):
        data = super(DetailsProductView, self).get_context_data(**kwargs)
        data['images'] = Product.objects.filter(active=True).first()
        return data


