from django.contrib import admin

from resume.models import MyProfile, Job, Education, Skill

admin.site.register(MyProfile)
admin.site.register(Job)
admin.site.register(Skill)
admin.site.register(Education)

