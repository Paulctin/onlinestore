from django.urls import path

from resume import views

urlpatterns = [
    path('resume/', views.ResumeTemplateView.as_view(), name='resume')
]
