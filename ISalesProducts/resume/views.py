from django.shortcuts import render
from django.views.generic import TemplateView

from resume.models import MyProfile, Job, Skill, Education


class ResumeTemplateView(TemplateView):
    template_name = 'resume/resume.html'

    def get_context_data(self, **kwargs):
        data = super(ResumeTemplateView, self).get_context_data(**kwargs)
        data['my_profile'] = MyProfile.objects.filter(active=True).first()
        data['my_jobs'] = Job.objects.filter(active=True)
        data['skills'] = Skill.objects.all()
        data['education'] = Education.objects.all()

        return data
