from django.db import models



class MyProfile(models.Model):
    name = models.CharField(max_length=50, null=True)
    email = models.EmailField(max_length=60, null=True)
    phone = models.CharField(max_length=14, null=True)
    gitlab = models.CharField(max_length=50, null=True)
    linkedin = models.CharField(max_length=100, null=True)
    image = models.FileField(upload_to='photos/', null=True)

    active = models.BooleanField(default=False, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.name


class Job(models.Model):
    role = models.CharField(max_length=60, null=True)
    company = models.CharField(max_length=100, null=True)
    start_date = models.DateField(null=True)
    end_date = models.DateField(blank=True, null=True)
    description = models.TextField(max_length=500, null=True)

    active = models.BooleanField(default=False, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.role


class Skill(models.Model):
    skill_name = models.CharField(max_length=50, null=True)
    description = models.TextField(max_length=300, null=True)

    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.skill_name


class Education(models.Model):
    school_name = models.CharField(max_length=100, null=True)
    profile = models.CharField(max_length=100, null=True)
    start_date = models.DateField(null=True)
    end_date = models.DateField(blank=True, null=True)

    active = models.BooleanField(default=False, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    update_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.school_name


