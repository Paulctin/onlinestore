from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView, DeleteView, DetailView

from category.forms import CategoryForm, CategoryUpdateForm
from category.models import Category
from product.models import Product


class CreateCategoryView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'category/create_category.html'
    model = Category
    form_class = CategoryForm
    success_url = reverse_lazy('list-of-category')
    permission_required = 'category.add_category'


    def form_valid(self, form):
        if form.is_valid() and not form.errors:
            new_category = form.save(commit=False)
            new_category.name = new_category.name.title()
            new_category.save()

        return redirect('create-category')


class CategoryListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'category/list_of_category.html'
    model = Category
    context_object_name = 'categories'
    permission_required = 'category.view_category'


class UpdateCategoryView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'category/update_category.html'
    model = Category
    form_class = CategoryUpdateForm
    success_url = reverse_lazy('list-of-category')
    permission_required = 'category.change_category'


class DeleteCategoryView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'category/delete_category.html'
    model = Category
    success_url = reverse_lazy('list-of-category')
    permission_required = 'category.delete_category'


class DetailsCategoryView(LoginRequiredMixin, DetailView):
    template_name = 'category/details_category.html'
    model = Category



def get_all_products_per_category(request, pk):
    get_all_products_per_category = Product.objects.filter(category_id=pk)
    return render(request, 'category/products_per_category.html', {'all_products_per_category': get_all_products_per_category})

