from django import forms
from django.forms import TextInput

from category.models import Category


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = '__all__'

        widgets = {
            'name': TextInput(attrs={'placeholder': 'Please enter category name', 'class': 'form-control'})
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        get_name = cleaned_data.get('name')
        all_names = Category.objects.all()
        for category_name in all_names:
            if category_name.name == get_name:
                msg = 'Exista acest nume de categorie in baza de date'
                self._errors['name'] = self.error_class([msg])

        return cleaned_data


class CategoryUpdateForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = '__all__'

        widgets = {
            'name': TextInput(attrs={'placeholder': 'Please enter category name', 'class': 'form-control'})
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        get_name = cleaned_data.get('name')
        all_names = Category.objects.all()
        for category_name in all_names:
            if category_name.name == get_name:
                msg = 'Exista acest nume de categorie in baza de date'
                self._errors['name'] = self.error_class([msg])

        return cleaned_data
