from django.urls import path

from category import views

urlpatterns = [
    path('create_category/', views.CreateCategoryView.as_view(), name='create-category'),
    path('list_of_category/', views.CategoryListView.as_view(), name='list-of-category'),
    path('update_category/<int:pk>', views.UpdateCategoryView.as_view(), name='update-category'),
    path('delete_category/<int:pk>', views.DeleteCategoryView.as_view(), name='delete-category'),
    path('products_per_category/<int:pk>', views.get_all_products_per_category, name='products-per-category'),
]