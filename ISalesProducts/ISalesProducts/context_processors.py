from category.models import Category
from product.models import Product


def get_all_categories(request):
    return {'categories': Category.objects.all()}


